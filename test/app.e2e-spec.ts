import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, HttpStatus } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import * as mongoose from 'mongoose';
import {
  Payment,
  PaymentDocument,
} from '../src/payments/schemas/payment.schema';

let app: INestApplication;

beforeAll(async () => {
  const moduleFixture: TestingModule = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();

  app = moduleFixture.createNestApplication();
  await app.init();
  await mongoose.connect(
    'mongodb+srv://admin:admin@cluster0.golvw.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
  );
  await mongoose.connection.db.dropDatabase();
});

afterAll(async (done) => {
  await app.close();
  await mongoose.disconnect(done);
});

describe('AppController (e2e)', () => {
  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });
});

describe('AppController (e2e)', () => {
  it('should create a random payment', async () => {
    const payment = {
      token: 'secret_token',
    };
    return await request(app.getHttpServer())
      .post('/payments')
      .set('Accept', 'application/json')
      .send(payment)
      .expect(HttpStatus.CREATED)
      .expect(({ body }) => {
        expect(body).toHaveProperty('_id');
        expect(body).toHaveProperty('status');
      });
  });
});
