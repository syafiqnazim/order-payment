import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PaymentsModule } from './payments/payments.module';
import config from '../config';

@Module({
  imports: [MongooseModule.forRoot(config.MONGO_URI), PaymentsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
