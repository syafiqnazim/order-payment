import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, Document } from 'mongoose';

export type PaymentDocument = Payment & Document;

@Schema()
export class Payment {
  @Prop()
  token: string;

  @Prop()
  status: string;

  @Prop({ type: Date, default: Date.now() })
  createdAt: Date;
}

export const PaymentSchema = SchemaFactory.createForClass(Payment);
