import { Controller, Post, Body } from '@nestjs/common';
import { CreatePaymentDto } from './dto/create-payment.dto';
import { PaymentsService } from './payments.service';
import { Payment } from './schemas/payment.schema';

@Controller('payments')
export class PaymentsController {
  constructor(private readonly paymentsService: PaymentsService) {}

  @Post()
  async create(@Body() createPaymentDto: CreatePaymentDto): Promise<Payment> {
    if (createPaymentDto.token === 'secret_token') {
      createPaymentDto.status = Math.random() > 0.5 ? 'confirmed' : 'declined';
      return this.paymentsService.create(createPaymentDto);
    }
    createPaymentDto.status = 'invalid';
    createPaymentDto.token = null;
    return this.paymentsService.create(createPaymentDto);
  }
}
