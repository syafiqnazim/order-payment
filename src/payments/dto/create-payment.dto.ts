import { Date } from 'mongoose';

export class CreatePaymentDto {
  token: string;
  status: string;
  readonly createdAt: Date;
}
